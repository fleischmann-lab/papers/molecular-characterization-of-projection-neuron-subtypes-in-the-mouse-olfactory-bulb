#Code by Robin Attey
#robin_attey@brown.edu | robin.attey@gmail.com

import pickle
import numpy as np
import pandas as pd
import os
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
import scanpy as sc

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score

from functools import reduce


#helper function for reading PCA coordinates from SCT integration
def read_sim_st_pca(folder,sim_table,st_table): 
    #folder: filepath to folder where PCA coordinates from SCT integration are stored
    #sim_table: metadata dataframe for simulated nuclei
    #st_table: metadata dataframe for sn-R1/R2/R3 nulcei
	sim_pca = pickle.load(open(os.path.join(folder,'simulation_PCA.p'),'rb'))
	st_pca = pickle.load(open(os.path.join(folder,'standard_PCA.p'),'rb'))

	sim_pca_table = pd.DataFrame(sim_pca,index=sim_table.index)
	st_pca_table = pd.DataFrame(st_pca,index=st_table.index)
	return (sim_pca_table, st_pca_table) #returns PCA coordinates as dataframes

#train multiple LDA classifiers, quantify their accuracy, apply them, and plot the results
def lda_quant_classifier_matrixplot(train_data,to_classify_data,train_info,classify_by,to_classify_info,group_by,classify_by_names = None, group_by_names = None, 
	num_runs = 100, results_color = "Set1",bar_colors = "Set1",save_folder=None,save_name=None,width=10,bars=True):
	"""INPUTS
	train_data: dataframe to use as input when training and validating classifiers
	to_classify_data: dataframe of input data wihtout known labels to classify
	train_info: dataframe of metadata for training data, including the output label for training the classifiers 
	classify_by: which attribute (column) of training data metadata should be the output of the classifiers
	to_classify_info: dataframe of metadata for data to be classified
	group_by: attribute (column) of to_classify_info to use for grouping the classification results when plotting
	classify_by_names: optional ordered list of options for classifier output. Order will be used for plotting results.
	group_by_names: optionalordered list of options for grouping classifier results. Order will be used for plotting results.
	num_runs: how many classifiers to train and use 
	results_color: optional dictionary mapping classifier output options to colors to use for plotting 
	bar_colors: optional dictionary mapping options to colors to use for plotting bars along the side of the plot when grouping classifier results
	save_folder: optional filepath to folder where the plot shold be saved. If None, plot will not be saved.
	save_name: optional filename to use when saving the plot
	width: width of plot
	bars: whether to plot bars alongside figure indicating group of cells being classified
	"""

	#get names if weren't passed to function
	if classify_by_names is None:
		#if results_color is not None and type(results_color) is dict:
			#classify_by_names = list(results_color.keys()) #if provided an ordering of names in the dictionary for colors, use that ordering of names in presented matrix
		#else:
		classify_by_names = train_info[classify_by].unique()
	if group_by_names is None:
		if bar_colors is not None and type(bar_colors) is dict:
			group_by_names = list(bar_colors.keys()) #if provided an ordering of names in the dictionary for colors, use that ordering of names in presented matrix
		else:
			group_by_names = to_classify_info[group_by].unique()

	#run classifiers all the times, get data as matrix
	jaccard = [] #list of jaccard scores
	for i in range(0,num_runs):

		#train and validate classifier on training data
		LDA = LinearDiscriminantAnalysis()
		x_train, x_test, y_train, y_test = train_test_split(train_data,train_info[classify_by]) #random train-test split for each classifier
		LDA.fit(x_train,y_train)
		test_pred = LDA.predict(x_test)
		conf = pd.DataFrame(confusion_matrix(y_test, test_pred,labels = classify_by_names,normalize = 'true'), index = classify_by_names, columns = classify_by_names)
		jaccard.append(accuracy_score(y_test, test_pred)) #accuracy computes jaccard score for binary and multiclass classification
		
		#predict classify_by of standard, store results in a dataframe
		if i == 0:
			matrix_data = pd.DataFrame(LDA.predict(to_classify_data),index=to_classify_info.index)
		else:
			matrix_data[i] = LDA.predict(to_classify_data)

	#make matrix AnnData to use matrix_plot function
	label_matrix = matrix_data #with labels, not numbers, to be returned
	for i in range(0,len(classify_by_names)): 	#first, convert strings to (binary) float
		matrix_data = matrix_data.replace(classify_by_names[i],i)
		print(classify_by_names[i],i)       #print correspondence of labels and numbers in final matrix for future analysis
	print([results_color[name] for name in classify_by_names])

	#plot results with a seaborn heatmap
	matrix_data[group_by] = to_classify_info[group_by]
	sort_list = [group_by] #sorting by group membership
	sort_list.extend([i for i in range(0,num_runs)]) #also sorting by classifier results
	matrix_data = matrix_data.sort_values(by=sort_list)

	fig = plt.figure(figsize=(width,5))
	fig.subplots_adjust(left=0.5)

	matrix_ax = sns.heatmap(matrix_data[range(0,num_runs)], 
                            cmap=[results_color[name] for name in classify_by_names], 
                            cbar = False,xticklabels=False,yticklabels=False) #take all columns except group_by
	print(matrix_ax)

	#plot colorbars on side for different groups being classified
	#set group positions (to pass to colorbar function below)
	if bars: #if want to plot bars for group_by
		group_positions = []
		start = 0
		for value in group_by_names:
			end = len([cell for cell in matrix_data.index if matrix_data.loc[cell,group_by]==value])
			group_positions.append((start,start+end-1))
			start = start + end
		print(group_positions)
		print(len(matrix_data.index))
		__plot_gene_groups_colorbars(matrix_ax, group_positions, group_by_names, bar_colors.values(),orientation = 'left',bar_width=num_runs/50)

	plt.show()

	#save plot
	if save_folder is not None:
		if save_name is None:
			fname = os.path.join(save_folder,'LDA_matrix'+str(num_runs)+'_runs')
		else:
			fname = os.path.join(save_folder,save_name)
		print(fname)
		matrix_ax.get_figure().savefig(fname,format='png')

	plt.clf()

	return (jaccard,matrix_data,label_matrix) #in case want to use these results for something else

#given a results matrix from the above function, plot the results (without training new classifiers)
def plot_matrixplot(matrix_data,train_info,classify_by,to_classify_info,group_by,classify_by_names = None, group_by_names = None, num_runs = 100, results_color = "Set1",bar_colors = "Set1",save_folder=None,save_name=None,width=10):
	#inputs are the same as those above for simplicity

	#get names if weren't passed to function
	if classify_by_names is None:
		classify_by_names = train_info[classify_by].unique()
	if group_by_names is None:
		if bar_colors is not None and type(bar_colors) is dict:
			group_by_names = list(bar_colors.keys()) #if provided an ordering of names in the dictionary for colors, use that ordering of names in presented matrix
		else:
			group_by_names = to_classify_info[group_by].unique()

	#sort results
	matrix_data[group_by] = to_classify_info[group_by]
	sort_list = [group_by]
	sort_list.extend([i for i in range(0,num_runs)]) #also sorting by classifier results
	matrix_data = matrix_data.sort_values(by=sort_list)

	fig = plt.figure(figsize=(width,5))
	fig.subplots_adjust(left=0.5)

	#make heatmap
	matrix_ax = sns.heatmap(matrix_data[range(0,num_runs)], 
                            cmap=[results_color[name] for name in classify_by_names], 
                            cbar = False) #take all columns except group_by
	print(matrix_ax)

	#plot colorbars on side for different groups being classified
	#set group positions (to pass to colorbar function below)
	group_positions = []
	start = 0
	for value in group_by_names:
		end = len([cell for cell in matrix_data.index if matrix_data.loc[cell,group_by]==value])
		group_positions.append((start,start+end-1))
		start = start + end
	print(group_positions)
	print(len(matrix_data.index))
	__plot_gene_groups_colorbars(matrix_ax, group_positions, group_by_names, bar_colors.values(),orientation = 'left',bar_width=num_runs/50)

	plt.show()

	#save plot
	if save_folder is not None:
		if save_name is None:
			fname = os.path.join(save_folder,'LDA_matrix'+str(num_runs)+'_runs')
		else:
			fname = os.path.join(save_folder,save_name)
		print(fname)
		matrix_ax.get_figure().savefig(fname,format='png')

	plt.clf()

#Helper function for plotting bars along the side of matrixplots for classifier results
#Adapted from code by Anton Crombach
def __plot_gene_groups_colorbars(gene_groups_ax, group_positions, group_labels, group_colors,
    left_adjustment = -0.3, right_adjustment = 0.3, rotation = None, orientation = 'top',bar_width=0.5): 	#THIS FUNCTION IS ANTON'S CODE
    """Plot colored bars on the sides of the heatmap with labels (if given)."""
    import matplotlib.patches as patches
    from matplotlib.collections import PatchCollection

    # get the rectangle coordinates as lists of start and end positions
    left = [x[0] + left_adjustment for x in group_positions]
    right = [x[1] + right_adjustment for x in group_positions]


    # Make the rectangles
    rects = []
    if orientation == 'top':
        # rotate labels if any of them is longer than 4 characters
        if rotation is None and group_labels:
            if max([len(x) for x in group_labels]) > 4:
                rotation = 90
            else:
                rotation = 0
        for idx in range(len(left)):
            rects.append(patches.Rectangle((left[idx], 0), width=right[idx], height=0.5))

            try:
                group_x_center = left[idx] + float(right[idx] - left[idx]) / 2
                gene_groups_ax.text(group_x_center, 1.1, group_labels[idx],
                                    ha='center', va='bottom', rotation=rotation)
            except:
                pass
    else:
        top = left
        bottom = right
        # Plotting from top to bottom
        for idx in range(len(top)):
            rects.append(patches.Rectangle((0, top[idx]), width=bar_width, height=bottom[idx]))

            try:
                group_y_center = (top[idx] + bottom[idx]) / 2
                gene_groups_ax.text(0, group_y_center, group_labels[idx],
                    ha='right', va='center', rotation=0)
            except Exception as e:
                print('problems {}'.format(e))
                pass

    print(rects)

    gene_groups_ax.add_collection(PatchCollection(rects, facecolor=group_colors, edgecolor='white'))
           
    # No frame or axes
    gene_groups_ax.spines['right'].set_visible(False)
    gene_groups_ax.spines['top'].set_visible(False)
    gene_groups_ax.spines['left'].set_visible(False)
    gene_groups_ax.spines['bottom'].set_visible(False)
    gene_groups_ax.grid(False)
    
    # remove y ticks
    gene_groups_ax.tick_params(axis='y', left=False, labelleft=False)
    # remove x ticks and labels
    gene_groups_ax.tick_params(axis='x', bottom=False, labelbottom=False, labeltop=False)