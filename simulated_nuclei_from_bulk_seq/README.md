# Simulated nuclei from bulk RNA-seq data

Even if bulk RNA-seq data averages over any cell-level signal, it is better at picking up low-expressed mRNAs than sc/snRNA-seq. Here we use the two types of data in concert to simulate the mRNA content of projection neuron nuclei. We create such nuclei by using the regulons from our pySCENIC analysis to guide a random sampling of mRNAs from bulk data. One could call this approach *sim-seq*.
