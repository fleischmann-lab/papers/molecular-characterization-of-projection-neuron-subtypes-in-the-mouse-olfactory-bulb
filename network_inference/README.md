# pySCENIC 100 runs

On the basis of co-expression between genes, we inferred the regulatory network. To this end we used the popular tool pySCENIC. It is a three-step pipeline, where the first step is a stochastic search for transcription factors (TFs) that are good predictors of target gene expression. Due to the stochastic component, we followed the established best practice of doing 100 runs of the pipeline and keeping results (TF-target gene pairs) that occur in at least 80 runs.
