# Molecular characterization of projection neuron subtypes in the mouse olfactory bulb

Analysis and vizualization code for our study on olfactory bulb projection neurons in the mouse. These are mainly Jupyter notebooks, with the odd R and Python file. See the manuscript on eLife: https://elifesciences.org/articles/65445#metrics

Please contact Sara Zeppilli (sara_zeppilli@brown.edu), Robin Attey (robin_attey@alumni.brown.edu), or Anton Crombach (anton.crombach@inria.fr) for technical info.

This repository has four parts (folders):

1. **transcriptome_analysis**: standard snRNA-seq transcriptomic analysis of the projection neurons and code for generating manuscript figures 2 and 3. Additionally, the code was used to generate UMAPs in supplementary figures.

2. **network_inference**: using pySCENIC, we calculate 100 times the set of regulons and keep regulons that are found in at least 80 of them. The result is a high-confidence gene regulatory network that we can analyse further.

3. **network_analysis**: gene regulatory network analysis. Several notebooks are used to do a comparison with the transcriptome analysis, including finding back marker genes, etc.; to subdivide the transcriptome-based cell types; to calculate regulon modules; and to quantify gradients of activity.

4. **simulated_nuclei_from_bulk_seq**: also known as *sim-seq*. Using the regulons from pySCENIC, we simulate a single nuclei data set from the bulk RNA-seq data. We use this to train classifiers to predict projection targets in the brain.


# Setup 

Clone and go into the project folder:
```
git clone https://gitlab.com/fleischmann-lab/papers/molecular-characterization-of-projection-neuron-subtypes-in-the-mouse-olfactory-bulb.git
cd molecular-characterization-of-projection-neuron-subtypes-in-the-mouse-olfactory-bulb
```

To create the environment go inside the folder analysis type and use the .yml file with its specific name:
```
cd name_folder_analysis
conda env create -f name_of_the_environment_file.yml
```
Activate the environment using the name inside the `.yml` file:
```
conda activate name_of_environment_in_the_file
```
Add your new environment (kernel) in Jupyter:
```
python -m ipykernel install --user --name= name_of_environment_in_the_file
```
Start Jupyter lab
```
jupyter lab
```


